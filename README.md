## Description

Build a standard deployment with Ansible in Debian so your code becomes idempotent and you can easily learn your way through YAML and automation best practices.

- https://docs.ansible.com/ansible-core/devel/playbook_guide/playbooks_templating.html
- https://docs.ansible.com/ansible/latest/user_guide/vault.html
- https://docs.ansible.com/ansible-core/devel/playbook_guide/playbooks_variables.html

***
#### Setup
Replace the config files in `files/foo` with your own. Edit `passwd.yml` using _ansible-vault_ changing all the required fields for your target host user[s] that are required. 

Enable or disable the roles you require and then run `ansible-galaxy collection install -r requirements.yml`.



#### Role Variables
```
my_key_1: my_value_1 
my_key_2: my_value_2 
my_key_3: my_value_3 
my_key_4: my_value_4 
my_key_5: my_value_5 
```

#### Dependencies
The role expects a service to be running, all other roles meet their own dependencies.

#### Installation
If you have deployed the supplemental roles  in `requirements.yml` and amended the variables to your liking, you can then run. 

```bash
$ ansible-playbook --extra-vars '@passwd.yml' deploy.yml -K
```

```yaml
---
- hosts: all
  become: yes
  gather_facts: yes
  roles:
    - roles/ansible-role
```
